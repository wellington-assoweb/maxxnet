<?php
/*Template Name: Blog*/
?>
<?php get_header('home') ?>
<section class="bannerFull bg<?php echo $post->ID ?>" itemscope itemtype="http://schema.org/LiveBlogPosting">
	<div class="overlay"></div>
	<div class="tituloBread">
		<div class="container">
			<div class="row">
				<div class="col-md-12" >
					<h1 itemprop="headline"><?php the_title(); ?></h1>
					<meta itemprop="name" content="<?php the_title(); ?>" />
					<div class="breadcrumb">
						<?php if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb('<p id="breadcrumbs">','</p>');
						} ?>
					</div><!-- ./breadcrumb -->
				</div>
			</div>
		</div>
	</div>	
</section>
<section id="fullSearch" class="search-bar hide-bg">
	<div class="search-bg"></div> 
	<i class="zmdi zmdi-close search-close"></i>
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<form method="get" id="searchform" action="<?php bloginfo('home'); ?>">
					<div class="mdl-textfield mdl-js-textfield is-upgraded" data-upgraded=",MaterialTextfield">
						<p class="search-label">Apenas digite e dê um 'enter'!</p>
						<label class="mdl-textfield__label" for="search-blog"></label>
						<input class="mdl-textfield__input" type="text" id="search-blog" type="search" name="s">
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
<section class="postagens" itemprop="liveBlogUpdate" itemscope itemtype="http://schema.org/BlogPosting">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-9">
				<div class="postguide">
					<div class="meta">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-3">
								<div class="imgAuthor">
									<div class="img-circle thumbAuthorSingle"><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php echo get_avatar( get_the_author_meta( 'user_email' ), 100 ); ?></a>
									</div>
									<span class="autor"><?php the_author_posts_link(); ?></span>
								</div>
							</div>

							<div class="col-xs-12 col-sm-12 col-md-3">
								<div class="data">
									<i class="fa fa-calendar-o"></i> <?php the_time('j \d\e F \d\e Y'); ?>
									<time itemprop="datePublished" content="<?php echo get_the_time('c'); ?>"/>

								</div>
							</div>
							
							<div class="col-xs-12 col-sm-12 col-md-6">
								<div class="categoria">
									<i class="fa fa-bookmark-o"></i>
									<?php
										$categories = get_the_category();
										$output = '';
										if($categories){
											foreach($categories as $category) {
												$output .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "Veja os posts sobre %s" ), $category->name ) ) . '">#'.$category->cat_name.'</a>';
											}
										echo trim($output);
										}
										wp_reset_query();
									?>
								</div>
							</div>
						</div>
					</div>
					<div class="share">
						<?php echo do_shortcode('[jpshare]'); ?>
					</div>
					<div class="conteudo-post">
						<div class="thumbSingle">
							<?php if (has_post_thumbnail($post->ID )): ?>
								<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
									<img itemprop="thumbnail" src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" class="img-responsive">
							<?php endif; ?>
						</div><!-- ./thumbSingle -->

						<div itemprop="articleBody">
							<?php the_content(); ?>
						</div>					
					</div>
					
					<div class="authorInfo">
						<div class="row">
							<div class="autor col-xs-12 col-sm-12 col-md-3">
								<div class="thumbAuthorSingle2">
									<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php echo get_avatar( get_the_author_meta( 'user_email' ), 150 ); ?></a>
								</div>
							</div>
							<div class="descricaoAutor col-xs-12 col-sm-12 col-md-9">
								<div class="bio"><?php the_author_posts_link(); ?><br/><p><?php the_author_meta( 'description' ); ?></p></div>
								<div class="links">
									<ul class="redesSociais">
										<?php if ( get_the_author_meta( 'facebook' ) ) { ?>
											<li class="facebook"><a target="_blank" href="<?php echo get_the_author_meta('facebook')?>"><?php echo get_the_author_meta('display_name')?> no Facebook</a></li>
										<?php } ?>
										<?php if ( get_the_author_meta( 'twitter' ) ) { ?>
											<li class="twitter"><a target="_blank" href="https://twitter.com/<?php echo get_the_author_meta('twitter')?>"><?php echo get_the_author_meta('display_name')?> no Twitter</a></li>
										<?php } ?>
										<?php if ( get_the_author_meta( 'googleplus' ) ) { ?>
											<li class="googleplus"><a target="_blank" href="<?php echo get_the_author_meta('googleplus')?>"><?php echo get_the_author_meta('display_name')?> no Google Plus</a></li>
										<?php } ?>
										<?php if ( get_the_author_meta( 'instagram' ) ) { ?>
											<li class="instagram"><a target="_blank" href="<?php echo get_the_author_meta('instagram')?>"><?php echo get_the_author_meta('display_name')?> no Intagram</a></li>
										<?php } ?>
									 </ul>
								</div>
							</div><!-- ./descricaoAutor -->
						</div>
					</div><!-- ./authorInfo -->
					<div class="postRelated">
						<?php echo do_shortcode( '[jetpack-related-posts]' ); ?>
			        </div>
			        <div class="comentarios" id="disqus_thread">
						<?php disqus_embed('agenciaassoweb'); ?>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-3 ssf">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</section>
<?php get_footer('home'); ?>