<?php get_header(); /* Template name: Termos promocionais */  ?>
<?php include(TEMPLATEPATH . '/template-parts/titulo-comum.php'); ?>
<?php the_content(); ?>
<section class="termos-promocionais">
	<div class="my-container">
		<div class="row">
			<?php
				while ( have_rows('imagens_dos_termos') ) : the_row();
				$imagem = get_sub_field('imagem');

			?>
				<div class="box-aqurivos">
					<img src="<?php echo $imagem['url']?>" alt="">
				</div>
			<?php endwhile ?>
		</div>
	</div>
</section>
<?php get_footer(); ?>
<SCRIPT>
	<!-- Disable
	function disableselect(e){
		return false
	}

	function reEnable(){
		return true
	}

	//if IE4+
	document.onselectstart=new Function ("return false")
	document.oncontextmenu=new Function ("return false")
	//if NS6
	if (window.sidebar){
		document.onmousedown=disableselect
		document.onclick=reEnable
	}

	//-->
</script>