<section class="head-title capa-<?php echo $post->ID; ?>">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12">
				<div class="page-name">
					<?php if (is_404()){ ?>
							<h1><strong>404 - PÁGINA NÃO ENCONTRADA</strong></h1>
					<?php }elseif(is_search()){ ?>
						<h1>Você buscou por: <?php /* Search Count */ $allsearch = &new WP_Query("s=$s&showposts=-1"); $key = wp_specialchars($s, 1); $count = $allsearch->post_count; _e(''); _e('<span class="search-terms">'); echo $key; _e('</span>'); _e(' - '); echo $count . ' '; _e('Resultados'); wp_reset_query(); ?></h1>
					<?php }elseif(is_page()){ ?>
						<h1><?php the_title(); ?></h1>
					<?php }elseif(is_page() || is_single()){ ?>
						<h1><?php the_title(); ?></h1>
					<?php }elseif(is_category()){ ?>
						<h1><?php printf( __( ' %s', '' ), '' . single_cat_title( '', false ) . '' ); ?></h1>
					<?php }elseif(is_post_type_archive('contratos')){ ?>
						<h1>Contratos</h1>
					<?php } ?>
					<?php if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb('<p id="breadcrumbs">','</p>');
					} ?>
				</div>
			</div>
		</div>
	</div>
</section>