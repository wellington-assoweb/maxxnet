<style>
	<?php
		if (is_front_page()):
 		elseif(is_page() || is_404()):
			$banner_comum = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
			if ($banner_comum[0]){
				echo '.head-title.capa-'.$post->ID.'{background-image: url("'.$banner_comum[0].'")}';
			}else{
				echo '.head-title.capa-'.$post->ID.'{background-image: url("'.THEMEURL.'/assets/img/default.jpg")}';
			}
		elseif(is_post_type_archive('contratos') || is_singular('contratos')):
			$banner_comum = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
			if ($banner_comum[0]){
				echo '.head-title.capa-'.$post->ID.'{background-image: url("'.$banner_comum[0].'")}';
			}else{
				echo '.head-title.capa-'.$post->ID.'{background-image: url("'.THEMEURL.'/assets/img/bg-head-central-do-assinante.jpg")}';
			}
		endif;
		?>
</style>

