<section class="blog-title">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="page-name">
					<?php if (is_home()){ ?>
						<h1>Blog do Cadabra <?php if ( $paged < 2 ) { } else { echo ' - Página '.$paged;} ?></h1>
					<?php }elseif(is_author()){ ?>
						<h1>Artigos de <?php the_author(); ?></h1>
					<?php }elseif(is_category()){ ?>
						<h1><?php printf( __( ' %s', '' ), '' . single_cat_title( '', false ) . '' ); ?></h1>
					<?php }else{ ?>
						<span class="blogui">Blog do Cadabra <?php if ( $paged < 2 ) { } else { echo ' - Página '.$paged;} ?></span>
					<?php } ?>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-8">
				<div class="menu-blog">
					<?php
					$categories = get_categories();
					$totalCategorias= count($categories);
					if($totalCategorias > 0){?>
						<ul>
							<li class="labely">
								<span>Categorias</span>
							</li>
							<?php
							$i = 0;
							foreach ($categories as $cat) {
								$option = '<li><a href="'.get_bloginfo('url').'/categorias-do-blog/'.$cat->category_nicename.'">';//Link da categoria
								$option .= '<i class="icon-dot-circled"></i> '.$cat->cat_name;//Nome da categoria
								$option .= '</a></li>';
								echo $option;//Imprime categorias
								if (++$i == 5) break; //Limita a iteração
							} ?>
							<li class="pull-right busca">
								<form class="searchbox" id="searchform" method="get" action="<?php bloginfo('home'); ?>">
									<input class="sb-search-input searchbox-input" placeholder="Faça sua busca" type="search" value="" name="s" id="s" onkeyup="buttonUp();">
									<input class="sb-search-submit searchbox-submit" type="submit" value="">
									<span class="sb-icon-search searchbox-icon">Busque <i class="icon-search"></i></span>
								</form>
							</li>
						</ul>
					<?php }else{ ?>
					<?php } wp_reset_postdata(); ?>
				</div>
				<div class="responsive-menu-blog">
					<?php
					$argumentos = array(
						'orderby'          => 'date',
						'order'            => 'DESC',
						'posts_per_page'   => 3,
						'caller_get_posts' => 1
					);
					$the_query = new WP_Query($argumentos);
					if ( $the_query->have_posts() ) {
						echo '
						<dropdown class="dropdown">
							<input id="toggle2" type="checkbox" />
							<label for="toggle2" class="animate">Categorias <i class="icon-down-open pull-right"></i></label>
							<ul class="animate">';
								$i = 0;
								foreach ($categories as $cat) {
									$option = '<li><a href="'.get_bloginfo('url').'/categorias-do-blog/'.$cat->category_nicename.'">';//Link da categoria
									$option .= '<i class="icon-dot-circled"></i> '.$cat->cat_name;//Nome da categoria
									$option .= '</a></li>';
									echo $option;//Imprime categorias
									if (++$i == 6) break; //Limita a iteração
								}
								/*echo '
								<li class="busca">
									<form id="sb-search" class="navbar-form responsive" method="get" id="searchform" action="'. bloginfo('home') .'">
										<div class="search-box form-group">
											<input class="search-input" type="text" placeholder="Pesquise no site" type="search" name="s" id="s">
											<input class="search-button" type="submit" value="">
											<i class="search-icon icon-search"></i>
										</div>
							        </form>
								</li>';*/
							echo '</ul>
						</dropdown>';
					} else {
					// no posts found
					} ?>
					<?php
					 /* Restore original Post Data */
					wp_reset_postdata(); ?>
				</div>
			</div>
			<div class="col-xs-12">
				<?php if (function_exists('yoast_breadcrumb')) {
				 	yoast_breadcrumb('<p id="breadcrumbs">','</p>');
		        }?>
			</div>
		</div>
	</div>
</section>