<?php get_header('comum'); ?>
<?php $name_page = $post->post_name; ?>
<section class="desafios">
    <div class="my-container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6">
                <div class="linha">
                    <div class="desc">
                        <?php the_content(); ?>
                    </div>
                    <div class="caixa">
                        <div class="know oto" id="clicarte">
                            <a href="#" class="solicitaContato">Solicite uma proposta</a>
                        </div>
                        <div class="know">
                            <a href="#Lmore" id="more-up">Conheça a solução</a>
                        </div>
                    </div>
                    <div class="challenges">
                        <h2>Desafios</h2>
                        <?php
                        if( get_field('desafios') ){ ?>
                            <ul>
                                <?php while ( has_sub_field('desafios') ) : ?>
                                    <?php if( get_row_layout() == 'desaf' ){?>
                                        <li><?php the_sub_field('desc_desaf'); ?></li>
                                    <?php } ?>
                                <?php endwhile; ?>
                            </ul>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6">
                <?php if( have_rows('slide_solution') ){ ?>
                    <div class="img-challeng">
                        <?php while( have_rows('slide_solution') ): the_row();
                            $title = get_sub_field('titulo_imagem');
                            $image = get_sub_field('imagem_solut');
                        ?>
                            <div class="item" style="background-image: url('<?php echo $image; ?>');">
                                <div class="over"></div>
                                <?php if ($title): ?>
                                    <div class="texto">
                                        <h3>
                                            <?php echo $title; ?>
                                        </h3>
                                    </div>
                                <?php endif ?>
                            </div>
                        <?php endwhile; ?>
                    </div>
                <?php }else{ ?>
                    <div class="img-alone">
                        <img src="<?php the_field('img_desafios'); ?>" alt="<?php the_title(); ?>">
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>

<section class="beneficios">
    <div class="my-container">
        <div class="row">
            <div class="col-xs-12">
                <div class="title">
                    <h3>Principais Benefícios e Funcionalidades do <span><?php if(get_field('nome_home', $queried_post->ID)){echo get_field('nome_home', $queried_post->ID);}else{echo $title;} ?></span></h3>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="benefits">
                    <?php
                        if( get_field('benefitio') ){
                            while ( has_sub_field('benefitio') ) : ?>
                                <?php if( get_row_layout() == 'all_beneficios' ){?>
                                    <div class="one-benefit">
                                        <div class="hex">
                                            <div class="inner">
                                                <img class="invert-it" src="<?php the_sub_field('icon_benefit'); ?>" alt="<?php the_title(); ?>">
                                            </div>
                                            <div class="corner-1"></div>
                                            <div class="corner-2"></div>
                                        </div>
                                        <div class="legend">
                                            <span><?php the_sub_field('beneficio'); ?></span>
                                        </div>
                                    </div>
                                <?php }
                            endwhile;
                        }
                    ?>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="box">
                    <div class="know oto" id="clicarte">
                        <a href="#" class="solicitaContato">Solicite uma proposta</a>
                    </div>
                    <div class="know">
                        <a href="#Lmore" id="more-up">Conheça a solução</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php if( get_field('estatisticas_sol') ){ ?>
    <section class="statistics">
        <div class="my-container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="title">
                            <h2>Estatísticas</h2>
                        </div>
                        <div class="breve-desc">
                            <?php the_field('text_estatisticas'); ?>
                        </div>
                    </div>
                </div>
                <?php
                $i = 0.1;
                while ( has_sub_field('estatisticas_sol') ) : ?>
                <?php if( get_row_layout() == 'all_estatisticas' ){?>
                    <div class="col-xs-12 col-sm-4 col-md-3 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="<?php echo $i; ?>s">
                        <div class="one-stats">
                            <div class="txt">
                                <div class="box">
                                    <h3><?php the_sub_field('title_estatistica'); ?></h3>
                                </div>
                            </div>
                            <?php if (get_sub_field('percentagem')): ?>
                                <div class="num perc">
                                    <div class="box">
                                        <span class="count"><?php the_sub_field('total_estaistica'); ?></span><span class="percent">%</span>
                                    </div>
                                </div>
                                <div class="unidade">
                                    <span><?php the_sub_field('texto_percentagem'); ?></span>
                                </div>
                            <?php else: ?>
                                <div class="num">
                                    <span class="count"><?php the_sub_field('total_estaistica'); ?></span>
                                </div>
                                <div class="unidade">
                                    <span><?php the_sub_field('texto_percentagem'); ?></span>
                                </div>
                            <?php endif ?>
                        </div>
                    </div>
                <?php }
                $i = $i + 0.1;
                endwhile;
                ?>
                <div class="col-xs-12">
                    <div class="tapa">
                        <div class="boxer">
                            <div class="know oto" id="clicarte">
                                <a href="#" class="solicitaContato">Solicite uma proposta</a>
                            </div>
                            <div class="know">
                                <a href="#Lmore" id="more-up">Conheça a solução</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php } ?>

<section class="clientes">
    <div class="my-container">
        <div class="row">
            <div class="col-xs-12">
                <div class="title">
                    <h2>Clientes</h2>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-1 col-lg-10">
                <div class="slide-clients">
                    <?php
                    $clientes = get_field('clientes_solucao');
                    if( $clientes ): ?>
                        <?php foreach( $clientes as $cliente): ?>
                            <?php setup_postdata($cliente); ?>
                            <?php $img_post = wp_get_attachment_image_src( get_post_thumbnail_id($cliente->ID), 'full' );
                            $url_img = $img_post['0']; ?>
                            <div class="item client-<?php echo $post->ID;?>">
                                <img class="img-responsive" src="<?php echo $url_img ?>" alt="<?php the_title(); ?>">
                            </div>
                        <?php endforeach; ?>
                        <?php wp_reset_postdata(); ?>
                    <?php endif;
                    ?>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="bots">
                    <div class="know">
                        <a href="<?php echo SITEURL;?>/clientes/#<?php echo $name_page; ?>">Conheça nossos clientes</a>
                    </div>
                   <!--  <div class="know cli">
                        <a href="#Lmore" id="more-up">Conheça a solução</a>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</section>


<?php
$cases = array(
    'post_type' => 'case',
    'posts_per_page' => -1,
    'orderby' => 'rand',
    'meta_query'    => array(
        array(
            'key'       => 'solucoes',
            'value'     => get_the_id(),
            'compare'   => 'LIKE'
        )
    )
);
$case = new WP_Query( $cases ); ?>

<?php
$qtd_posts = $case->found_posts; ?>

<?php
if( $qtd_posts ):
    setup_postdata( $case );  ?>
    <?php while ( $case->have_posts() ) : $case->the_post();?>
        <section class="case-of">
            <?php $img_post = wp_get_attachment_image_src( get_post_thumbnail_id($case->ID), 'full' );
            $url_img = $img_post['0']; ?>
            <?php
            $img_title = wp_get_attachment_image_src( get_post_thumbnail_id($case->ID), 'full' );
            $url_img_title = $img_title['0']; ?>
            <?php if ($img_title){ ?>
                <div class="o-case" style="background-image:url('<?php echo $url_img ?>')">
            <?php }else{ ?>
                <div class="o-case" style="background-image:url('<?php echo THEMEURL; ?>/assets/img/default.jpg')">
            <?php } ?>
                <div class="overlayer"></div>
                <div class="my-container">
                    <div class="row">
                        <div class="col-xs-12 dvi col-sm-6 col-md-5">
                            <div class="about-case">
                                <div class="titulo">
                                    <h2><?php the_title(); ?></h2>
                                </div>
                                <div class="desc">
                                    <?php the_field('texto_para_solucoes'); ?>
                                </div>
                                <div class="know">
                                    <a href="<?php the_permalink(); ?>">Saiba mais</a>
                                </div>
                                <div class="know mores">
                                    <a href="<?php echo SITEURL;?>/cases/#<?php echo $name_page; ?>">conheça nossos cases</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 dvi col-sm-6 col-md-7">
                            <?php if (get_field('video_do_case')): ?>
                                <div class="play">
                                    <a href="https://www.youtube.com/watch?v=<?php the_field('video_do_case') ?>">
                                        <i class="icon-youtube-play"></i>
                                    </a>
                                </div>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        <?php break; ?>
    <?php endwhile;?>

    <?php
    wp_reset_postdata();
    wp_reset_query();
    ?>
<?php endif; ?>

<?php if( have_rows('entidades_reg') ){ ?>
    <section class="clientes">
        <div class="my-container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="title">
                        <h2>Padrão Nacional e Internacional</h2>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-1 col-lg-10">
                    <div class="slide-clients">
                        <?php while( have_rows('entidades_reg') ): the_row();
                            $nome = get_sub_field('nome_entidade');
                            $image = get_sub_field('logo_entidade');
                        ?>
                            <div class="item client-<?php echo $post->ID;?>">
                                <img class="img-responsive" src="<?php echo $image ?>" alt="<?php echo $nome; ?>">
                            </div>
                        <?php endwhile; ?>
                    </div>
                </div>
                <!-- <div class="col-xs-12">
                    <div class="bots">
                        <div class="know">
                            <a href="<?php echo SITEURL;?>/clientes/#<?php echo $post->post_name; ?>">Conheça nossos clientes</a>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </section>
<?php } ?>



<section class="hideContent">
    <a class="link-scrool" name="Lmore"></a>
    <div class="over"></div>
    <div class="bgline">
        <div class="over"></div>
        <?php if( get_field('modulos_sistema') ){ ?>
            <div class="modulos">
                <div class="my-container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="title">
                                <h2>Módulos do <?php if(get_field('nome_home', $queried_post->ID)){echo get_field('nome_home', $queried_post->ID);}else{echo $title;} ?></h2>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="box-abs">
                                <?php while ( has_sub_field('modulos_sistema') ) : ?>
                                        <div class="itemmod">
                                            <div class="um-modulo">
                                                <?php if( get_row_layout() == 'detalhes_modulo' ){?>
                                                    <div class="imagi">
                                                        <img src="<?php the_sub_field('icone_modulo'); ?>" alt="<?php the_title(); ?>" class="img-responsive">
                                                    </div>
                                                    <div class="nome">
                                                        <span><?php the_sub_field('titulo_modulo'); ?></span>
                                                    </div>
                                                <?php } ?>
                                            </div>

                                            <div class="info">
                                                <div class="col-sm-3 borda">
                                                    <img src="<?php the_sub_field('icone_modulo'); ?>" alt="<?php the_title(); ?>" class="img-responsive">
                                                </div>
                                                <div class="col-sm-9 borda2">
                                                    <h5><?php the_sub_field('titulo_modulo'); ?></h5>
                                                    <?php the_sub_field('descricao_modulo'); ?>
                                                </div>
                                                <a href="#" id="closss"><i class="icon-cancel-5"></i></a>
                                            </div>
                                        </div>
                                <?php endwhile; ?>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="know">
                                <a href="#" class="solicitaContato">Solicite uma proposta</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php if (get_field('imagem_complementar')): ?>
            <div class="more-about">
                <div class="my-container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-offset-2 col-sm-8">
                            <div class="titulo">
                                <h2><?php echo get_field('nome_home', $queried_post->ID); ?></h2>
                            </div>
                            <div class="desc">
                                <?php the_field('mais_sobre'); ?>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="image">
                                <img class="img-responsive" src="<?php the_field('imagem_complementar'); ?>" alt="<?php the_title(); ?>">
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="know">
                                <a href="#" class="solicitaContato">Solicite uma proposta</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif ?>
    </div><!-- /.bgline -->

    <div class="principais">
        <div class="my-container">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="title">
                        <h2>Principais características do <?php if(get_field('nome_home', $queried_post->ID)){echo get_field('nome_home', $queried_post->ID);}else{echo $title;} ?></h2>
                    </div>
                    <div class="caracteristicas">
                        <?php
                        if( get_field('principais_caracteristicas') ){ ?>
                            <ul>
                                <?php while ( has_sub_field('principais_caracteristicas') ) : ?>
                                    <?php if( get_row_layout() == 'caracteristica' ){?>
                                        <li><?php the_sub_field('brev_desc') ?></li>
                                    <?php } ?>
                                <?php endwhile;?>
                            </ul>
                        <?php } ?>
                    </div>
                    <div class="know">
                        <a href="#" class="solicitaContato">Solicite uma proposta</a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="image mm">
                        <img src="<?php the_field('icone_solucao'); ?>" alt="<?php the_title(); ?>">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    $posts = get_field('solucoes_complementares');
    if( $posts ): ?>
    <div class="other-solutions">
        <div class="my-container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="title">
                        <h2>Soluções complementares</h2>
                    </div>
                </div>
                <?php $cont = 1 ?>
                <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
                    <?php setup_postdata($post); ?>
                    <?php if ($cont <= 2): ?>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                    <?php else: ?>
                        <div class="col-xs-12 col-sm-offset-3 col-sm-6 col-md-offset-0 col-md-4">
                    <?php endif ?>
                        <?php $img_post = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' );
                        $url_img = $img_post['0']; ?>
                        <div class="one-solution" style="background-image:url('<?php echo $url_img ?>')">
                            <div class="overlay"></div>
                            <div class="box">
                                <div class="title">
                                    <h3><?php the_title(); ?></h3>
                                </div>
                                <div class="preview">
                                    <p><?php the_content_limit(130); ?></p>
                                </div>
                                <div class="know">
                                    <a href="<?php the_permalink(); ?>">Conheça a solução</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $cont++; ?>
                <?php endforeach; ?>
                <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

                <div class="col-xs-12">
                    <div class="more-solutions">
                        <div class="know">
                            <a href="<?php echo SITEURL;?>/solucoes">Mais soluções</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php endif;?>
</section>
<div class="know abs-btn">
    <a href="#Lmore" id="more-up"><i class="icon-angle-down"></i> Conheça a solução <?php //if(get_field('nome_home', $queried_post->ID)){echo get_field('nome_home', $queried_post->ID);}else{echo $title;} ?></a>

</div>

<!-- MODAL VÍDEOS -->
<div class="modal the-mod fade" id="video" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="embed-responsive embed-responsive-16by9">
                    <div id="player"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="icon-cancel-1"></i></button>
            </div>
        </div>
    </div>
</div>

<!-- Preenche campo hidden -->
<?php $nome = get_the_title(); ?>
<script>
    (function($) {
        $('#titulo').val('<?php echo $nome ?>');
    })(jQuery);

    (function() {
        // trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
        if (!String.prototype.trim) {
            (function() {
                // Make sure we trim BOM and NBSP
                var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
                String.prototype.trim = function() {
                    return this.replace(rtrim, '');
                };
            })();
        }

        [].slice.call( document.querySelectorAll( 'input.input__field' ) ).forEach( function( inputEl ) {
            // in case the input is already filled..
            if( inputEl.value.trim() !== '' ) {
                classie.add( inputEl.parentNode, 'input--filled' );
            }

            // events:
            inputEl.addEventListener( 'focus', onInputFocus );
            inputEl.addEventListener( 'blur', onInputBlur );
        } );

        [].slice.call( document.querySelectorAll( 'textarea.input__field' ) ).forEach( function( inputEl ) {
            // in case the input is already filled..
            if( inputEl.value.trim() !== '' ) {
                classie.add( inputEl.parentNode, 'input--filled' );
            }

            // events:
            inputEl.addEventListener( 'focus', onInputFocus );
            inputEl.addEventListener( 'blur', onInputBlur );
        } );

        function onInputFocus( ev ) {
            classie.add( ev.target.parentNode, 'input--filled' );
        }

        function onInputBlur( ev ) {
            if( ev.target.value.trim() === '' ) {
                classie.remove( ev.target.parentNode, 'input--filled' );
            }
        }
    })();
</script>
<?php get_footer(); ?>