<?php
/*=========================================================================================
LOAD SCRIPTS AND STYLES
=========================================================================================*/
add_action('wp_enqueue_scripts', 'sample_scripts');
function sample_scripts() {
  //PEGA O DIRETÓRIO DEFAULT DOS ARQUIVOS PARA CONCATENAR
  define(CSS_PATH, get_template_directory_uri().'/assets/css/');
  define(JS_PATH, get_template_directory_uri().'/assets/js/');

  wp_enqueue_script('jquery');

  /*Fonts*/
  wp_register_style('open-sans', '//fonts.googleapis.com/css?family=Open+Sans:400,400italic', null, null, 'all' );
  wp_enqueue_style('open-sans');
  wp_register_style('fontello', CSS_PATH.'fontello.min.css', null, null, 'all' );
  wp_enqueue_style('fontello');
  /*Fim Fonts*/

  //Declaração de scripts
  wp_register_script('bootstrap_js', JS_PATH.'bootstrap.min.js', null, null, true );
  wp_register_script('easing', JS_PATH.'jquery.easing.min.js', null, null, true );
  wp_register_script('wow-js', JS_PATH.'wow.min.js', null, null, true );

  wp_register_script('smooth-scroll', JS_PATH.'smooth-scroll.min.js', null, null, true );
  //wp_register_script('dropdown-js', JS_PATH.'hover-dropdown.min.js', null, null, true );
  wp_register_script('scripts', JS_PATH.'scripts.min.js', null, null, true );

  wp_enqueue_script('easing');
  wp_enqueue_script('bootstrap_js');
  wp_enqueue_script('smooth-scroll');
  //wp_enqueue_script('dropdown-js');

  //Declaração de estilos
  wp_register_style('bootstrap', CSS_PATH.'bootstrap.min.css');
  wp_register_style('style', CSS_PATH.'style.min.css', null, null, 'all' );
  wp_register_style('animate-css', CSS_PATH.'animate.min.css', null, null, 'all' );
  wp_register_style('wordpress-css', CSS_PATH.'wordpress.min.css', null, null, 'all' );

  //Chamada de estilos
  wp_enqueue_style('bootstrap');
  wp_enqueue_style('animate-css');
  wp_enqueue_style('wordpress-css');
  wp_enqueue_style('style');

  /*MENU*/
  wp_register_script('classie', JS_PATH.'classie.min.js', null, null, true );
  //wp_register_script('main', JS_PATH.'main.min.js', null, null, true );
  wp_enqueue_script('classie');

  /*FIM MENU*/

  if(is_front_page()){
    wp_register_style('front-page-css', CSS_PATH.'front-page.min.css', null, null, 'all' );
    wp_enqueue_style('front-page-css');

    wp_register_script('home-js', JS_PATH.'home.min.js', null, null, true );
    wp_enqueue_script('home-js');

  }elseif(is_page(42) || is_page(65) || is_page(173)){ // PARA VOCÊ, PARA SUA EMPRESA e MAXX RADIO
    wp_register_style('planos-css', CSS_PATH.'planos.min.css', null, null, 'all' );
    wp_enqueue_style('planos-css');

    if(is_page(65)){ // PARA SUA EMPRESA
      wp_register_style('para-sua-empresa-css', CSS_PATH.'para-sua-empresa.min.css', null, null, 'all' );
      wp_enqueue_style('para-sua-empresa-css');
    }
    if(is_page(42)  || is_page(173)){ // PARA VOCÊ, MAXX RADIO
      wp_register_style('para-voce-css', CSS_PATH.'para-voce.min.css', null, null, 'all' );
      wp_enqueue_style('para-voce-css');

    }

  }elseif(is_page(96)){ // CENTRAL DO ASSINANTE
    wp_register_style('contato-css', CSS_PATH.'contato.min.css', null, null, 'all' );
    wp_enqueue_style('contato-css');

    wp_register_style('central-do-assinante-css', CSS_PATH.'central-do-assinante.min.css', null, null, 'all' );
    wp_enqueue_style('central-do-assinante-css');


  }elseif(is_page(100)){ // SOBRE
    wp_register_style('sobre-css', CSS_PATH.'sobre.min.css', null, null, 'all' );
    wp_enqueue_style('sobre-css');

  }elseif(is_page(106) || is_page(259)){ // CONTATO
    wp_register_style('contato-css', CSS_PATH.'contato.min.css', null, null, 'all' );
    wp_enqueue_style('contato-css');

    wp_register_script('jquery.mask.min-js', JS_PATH.'jquery.mask.min.js', null, null, true );
    wp_enqueue_script('jquery.mask.min-js');

  }elseif(is_page(352)){ // PROGRAMA INDICOU GANHOU
    wp_register_style('programa-indicou-ganhou-css', CSS_PATH.'programa-indicou-ganhou.min.css', null, null, 'all' );
    wp_enqueue_style('programa-indicou-ganhou-css');

    wp_register_style('contato-css', CSS_PATH.'contato.min.css', null, null, 'all' );
    wp_enqueue_style('contato-css');

    wp_register_script('jquery.mask.min-js', JS_PATH.'jquery.mask.min.js', null, null, true );
    wp_enqueue_script('jquery.mask.min-js');

  }elseif(is_page(356)){ // TESTE SUA VELOCIDADE
    wp_register_style('teste-sua-velocidade-css', CSS_PATH.'teste-sua-velocidade.min.css', null, null, 'all' );
    wp_enqueue_style('teste-sua-velocidade-css');
  }elseif(is_404()){
      wp_register_style('404-css', CSS_PATH.'404.min.css', null, null, 'all' );
      wp_enqueue_style('404-css');

  }elseif(is_post_type_archive() || is_singular()){

    if (is_post_type_archive()) {}

    if (is_singular('planos-empresariais')) {
      wp_register_style('single-empresariais', CSS_PATH.'single-empresariais.min.css', null, null, 'all' );
      wp_enqueue_style('single-empresariais');

    }elseif(is_singular('contratos') || is_post_type_archive('contratos')){ // Contratos
      wp_register_style('contratos', CSS_PATH.'contratos.min.css', null, null, 'all' );
      wp_enqueue_style('contratos');
    }

  }


  //Chamada de scripts
  wp_enqueue_script('scripts');
  wp_enqueue_script('wow-js');

}

// Aumenta limite de upload
@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );

/*ADICIONA FAVICON À TELA DE LOGIN E AO PAINEL DO SITE*/
add_action( 'login_head', 'favicon_admin' );
add_action( 'admin_head', 'favicon_admin' );
function favicon_admin() {
    $favicon_url = get_template_directory_uri() . '/assets/img/favicon';
    $favicon  = '<!-- Favicon IE 9 -->';
    $favicon .= '<!--[if lte IE 9]><link rel="icon" type="image/x-icon" href="' . $favicon_url . '.ico" /> <![endif]-->';
    $favicon .= '<!-- Favicon Outros Navegadores -->';
    $favicon .= '<link rel="shortcut icon" type="image/png" href="' . $favicon_url . '.png" />';
    $favicon .='<!-- Favicon iPhone -->';
    $favicon .='<link rel="apple-touch-icon" href="' . $favicon_url . '.png" />';
    echo $favicon;
}

//DISQUS
function disqus_embed($disqus_shortname) {
    global $post;
    wp_enqueue_script('disqus_embed','http://'.$disqus_shortname.'.disqus.com/embed.min.js');
    echo '<div id="disqus_thread"></div>
    <script type="text/javascript">
        var disqus_shortname = "'.$disqus_shortname.'";
        var disqus_title = "'.$post->post_title.'";
        var disqus_url = "'.get_permalink($post->ID).'";
        var disqus_identifier = "'.$disqus_shortname.'-'.$post->ID.'";
    </script>';
}



//CUSTOM POST TYPE Contratos
add_action('init', 'type_post_termos_promocionais');
function type_post_termos_promocionais() {
  $labels = array(
    'name'                => _x( 'Contratos', 'Post Type General Name', 'contratos' ),
    'singular_name'       => _x( 'contrato', 'Post Type Singular Name', 'contratos' ),
    'menu_name'           => __( 'Contratos', 'contratos' ),
    'parent_item_colon'   => __( 'Parent Item:', 'contratos' ),
    'all_items'           => __( 'Todos os Contratos', 'contratos' ),
    'view_item'           => __( 'Ver contrato', 'contratos' ),
    'add_new_item'        => __( 'Add novo contrato', 'contratos' ),
    'add_new'             => __( 'Add novo', 'contratos' ),
    'edit_item'           => __( 'Editar contrato', 'contratos' ),
    'update_item'         => __( 'Atualizar contrato', 'contratos' ),
    'search_items'        => __( 'Pesquisar contrato', 'contratos' ),
    'not_found'           => __( 'Nada encontrado', 'contratos' ),
    'not_found_in_trash'  => __( 'Nada encontrado na lixeira', 'contratos' ),
    );
  $rewrite = array(
    'slug'                => 'contratos'
    );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'public_queryable' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => $rewrite,
    'capability_type' => 'post',
    'has_archive' => true,
    'menu_icon' => 'dashicons-format-aside',
    'hierarchical' => false,
    'menu_position' => null,

    'supports' => array('title', 'editor', 'thumbnail', 'comments', 'revisions', 'trackbacks')
    );

    register_post_type( 'contratos' , $args );
    flush_rewrite_rules();
}


//FUNÇÃO PARA SUPORTE A UPLOAD DE IMAGENS SVG
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

//ADICIONA SUPORTE A MENUS NO PAINEL ADMIN
add_filter( 'show_admin_bar', '__return_false' );
add_theme_support('menus');

require_once('wp_bootstrap_navwalker.php');
//require_once('wlaker_dropdown.php');
register_nav_menus( array(
    'header'     => __('Header'),
    'header-responsivo'     => __('header-responsivo'),
    'footer-responsivo'     => __('footer-responsivo')
) );

add_filter('show_admin_bar', '__return_false');

/*=======================================================================================
ENABLE THUMBS
=======================================================================================*/
add_theme_support('post-thumbnails');

/*=======================================================================================
UNABLE LOGIN SHOW ERRORS
=======================================================================================*/
add_filter('login_errors',create_function('$a', "return null;"));

/*=======================================================================================
REMOVE HEAD VERSION
=======================================================================================*/
remove_action('wp_head', 'wp_generator');

/*=======================================================================================
ENABLE THUMBS
=======================================================================================*/
add_theme_support( 'post-thumbnails' );

/*=======================================================================================
ENABLE EXCERPTS
=======================================================================================*/
add_post_type_support('page', 'excerpt');



/*=======================================================================================
CUSTOM FUNCTION: LIMIT TEXT
=======================================================================================*/
function the_content_limit($max_char, $more_link_text = '<br/><i class="fa fa-smile-o"></i>', $stripteaser = 0, $more_file = '') {
    $content = get_the_content($more_link_text, $stripteaser, $more_file);
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]>', $content);
    $content = strip_tags($content);

   if (strlen($_GET['']) > 0) {
      echo "<p>";
      echo $content;
      echo "</p>";
   }
   else if ((strlen($content)>$max_char) && ($espacio = strpos($content, " ", $max_char ))) {
        $content = substr($content, 0, $espacio);
        $content = $content;
        echo "<p>";
        echo $content;
        echo '...';
        echo "</p>";
   }
   else {
      echo "<p>";
      echo $content;
      echo "</p>";
   }
}

/*MAIS LIDAS
======================*/
// Verifica se não existe nenhuma função com o nome post_count_session_start
if ( ! function_exists( 'post_count_session_start' ) ) {
    // Cria a função
    function post_count_session_start() {
        // Inicia uma sessão PHP
        if ( ! session_id() ) session_start();
    }
    // Executa a ação
    add_action( 'init', 'post_count_session_start' );
}

// Verifica se não existe nenhuma função com o nome tp_count_post_views
if ( ! function_exists( 'tp_count_post_views' ) ) {
    // Conta os views do post
    function tp_count_post_views () {
        // Garante que vamos tratar apenas de posts
        if ( is_single() ) {

            // Precisamos da variável $post global para obter o ID do post
            global $post;

            // Se a sessão daquele posts não estiver vazia
            if ( empty( $_SESSION[ 'tp_post_counter_' . $post->ID ] ) ) {

                // Cria a sessão do posts
                $_SESSION[ 'tp_post_counter_' . $post->ID ] = true;

                // Cria ou obtém o valor da chave para contarmos
                $key = 'tp_post_counter';
                $key_value = get_post_meta( $post->ID, $key, true );

                // Se a chave estiver vazia, valor será 1
                if ( empty( $key_value ) ) { // Verifica o valor
                    $key_value = 1;
                    update_post_meta( $post->ID, $key, $key_value );
                } else {
                    // Caso contrário, o valor atual + 1
                    $key_value += 1;
                    update_post_meta( $post->ID, $key, $key_value );
                } // Verifica o valor

            } // Checa a sessão

        } // is_single
        return;
      }
    add_action( 'get_header', 'tp_count_post_views' );
}

add_post_type_support('post', 'excerpt');