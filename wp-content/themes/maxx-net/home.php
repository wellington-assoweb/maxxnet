<?php /* Template name: Blog */ get_header(); ?>
<?php include(TEMPLATEPATH . '/template-parts/titulo-blog.php'); ?>

<section class="home-blog">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-9">
				<?php if ($paged < 2){ ?>
					<div class="row">
						<!-- DESTAQUES -->
						<?php
						$fixed = array(
							'type'             => 'post',
							'caller_get_posts' => 1,
							'posts_per_page'   => 1
						);
						$marcado;
						$the_query = new WP_Query($fixed);
						if($the_query->have_posts()) : while($the_query->have_posts()) {
							$the_query->the_post(); ?>
								<div class="col-xs-12">
									<article class="um-post maior">
										<?php $marcado = array($post->ID);?>
										<a href="<?php the_permalink() ?>" class="img-link">
											<?php if (has_post_thumbnail($post->ID )){
												$back = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
												$img_destak = $back['0']; ?>
												<div class="img-destak" style="background-image:url('<?php echo $img_destak ?>') !important">
											<?php }else{ ?>
												<div class="img-destak" style="background-image:url('<?php echo get_template_directory_uri(); ?>/assets/img/default.jpg')">
											<?php } ?>
												<div class="overlayer"></div>
												<div class="categoria">
													<?php
														$categories = get_the_category();
														$output = '';
														$cont = 0;//contador
														if($categories){
															foreach($categories as $category) {
																if ($cont == 1) {//Limita categorias
																	break;
																}
																$output .= '<a href="'.get_category_link( $category->term_id ).'" class="hvr-pulse-grow" >'.$category->cat_name.'</a>';
																$cont++;
															}
															echo trim($output);
														}
													?>
												</div><!-- /.categoria -->
											</div>
										</a>
									<div class="info-post">
										<h2 class="headline"><a href="<?php the_permalink() ?>"><?php the_title()?></a></h2>
										<?php the_content_limit(110);?>
									</div>
								</article>
							</div>
						<?php }
						endif; ?>
						<?php wp_reset_query();?>
					</div>
				<?php } ?>
				<div class="row">
					<!-- Demais Posts -->
					<?php
					$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
					$args = array(
						'post_type'      => 'post',
						'order'          => 'DSC',
						'post__not_in'   =>  $marcado,
						'posts_per_page' => 4,
						'paged'          => $paged
					);
						$the_query = new WP_Query($args);
						if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();?>
							<div class="col-sm-12 col-md-6">
								<article class="um-post menor">
									<a href="<?php the_permalink() ?>" class="img-link">
										<?php if (has_post_thumbnail($post->ID )){
											$back = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
											$img_destak = $back['0']; ?>
											<div class="img-destak" style="background-image:url('<?php echo $img_destak ?>') !important">
										<?php }else{ ?>
											<div class="img-destak" style="background-image:url('<?php echo get_template_directory_uri(); ?>/assets/img/default.jpg')">
										<?php } ?>
											<div class="overlayer"></div>
											<div class="categoria">
												<?php
													$categories = get_the_category();
													$output = '';
													$cont = 0;//contador
													if($categories){
														foreach($categories as $category) {
															if ($cont == 1) {//Limita categorias
																break;
															}
															$output .= '<a href="'.get_category_link( $category->term_id ).'" class="hvr-pulse-grow" >'.$category->cat_name.'</a>';
															$cont++;
														}
														echo trim($output);
													}
												?>
											</div><!-- /.categoria -->
										</div>
									</a>
									<div class="info-post">
										<h2 class="headline"><a href="<?php the_permalink() ?>"><?php the_title()?></a></h2>
										<?php the_content_limit(110);?>
									</div>
								</article>
							</div>

						<?php endwhile; ?>
							<div class="navegacao">
								<?php if(function_exists('wp_pagenavi')) { wp_pagenavi( array( 'query' => $the_query ) ); } ?>
							</div>
						<?php else: ?>
						<?php endif; ?>
					</div>
			</div>
			<div class="col-xs-12 col-sm-offset-3 col-sm-6 col-md-offset-0 col-md-3">
				<?php include(TEMPLATEPATH . '/template-parts/sidebar.php'); ?>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>