<?php /* Template name: Blog */ get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<?php include(TEMPLATEPATH . '/template-parts/titulo-blog.php'); ?>
	<section class="single-blog">
		<div class="my-container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-9">
					<div class="title">
						<h1><?php the_title(); ?></h1>
					</div>
					<div class="dados-post">
						<span class="authority">
							<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>">
								<?php echo get_avatar( get_the_author_meta( 'user_email' ), 150 ); ?>
							</a>
							<a class="name-author" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php the_author(); ?></a>
						</span>
						<span class="uma-cat">
							<?php
								$categories = get_the_category();
								$output = '';
								$cont = 0;//contador
								if($categories){
									foreach($categories as $category) {
										if ($cont == 1) {//Limita categorias
											break;
										}
										$output .= '<i class="icon-circle-empty"></i> <a href="'.get_category_link( $category->term_id ).'" class="hvr-pulse-grow" >'.$category->cat_name.'</a>';
										$cont++;
									}
									echo trim($output);
								}
							?>
						</span>
						<time><i class="icon-circle-empty"></i> <?php the_time('d/m/Y') ?></time>
					</div>
					<?php if (has_post_thumbnail($post->ID )){
						$back = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
						$img_destak = $back['0']; ?>
						<div class="img-single" style="background-image:url('<?php echo $img_destak ?>') !important">
					<?php }else{ ?>
						<div class="img-single" style="background-image:url('<?php echo get_template_directory_uri(); ?>/assets/img/default.jpg')">
					<?php } ?>
						<div class="categoria">
							<?php
								$categories = get_the_category();
								$output = '';
								$cont = 0;//contador
								if($categories){
									foreach($categories as $category) {
										if ($cont == 1) {//Limita categorias
											break;
										}
										$output .= '<a href="'.get_category_link( $category->term_id ).'" class="hvr-pulse-grow" >'.$category->cat_name.'</a>';
										$cont++;
									}
									echo trim($output);
								}
							?>
						</div><!-- /.categoria -->
					</div>
					<div class="content">
						<?php the_content(); ?>
					</div>
					<div class="comentarios" id="disqus_thread">
                        <?php disqus_embed('jrcadabra'); ?>
                    </div>
				</div>
				<div class="col-xs-12 col-sm-offset-3 col-sm-6 col-md-offset-0 col-md-3">
					<?php include(TEMPLATEPATH . '/template-parts/sidebar.php'); ?>
				</div>
			</div>
		</div>
	</section>
<?php endwhile?>
<?php else: ?>
<?php endif; ?>
<?php get_footer(); ?>