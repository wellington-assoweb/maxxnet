		</div><!-- /#wrap -->
		<footer class="footer">
			<img class="bg-left-footer" src="<?php echo THEMEURL ?>/assets/img/bg-left-footer.png" alt="">
			<img class="bg-right-footer" src="<?php echo THEMEURL ?>/assets/img/bg-right-footer.png" alt="">
			<div class="my-container">
				<div class="row">
					<div class="col-xs-12 col-md-7 col-lg-6">
						<nav class="menu-footer">
							<?php
								wp_nav_menu(
									array(
										'menu'           => 'Footer',
										'theme_location' => 'footer',
										'depth'          => 2,
										'container'      => '',
										'menu_id'        => 'one-header',
										'fallback_cb'    => 'wp_bootstrap_navwalker::fallback',
										'walker'         => new wp_bootstrap_navwalker()
									)
								);
							?>
						</nav>
						<nav class="menu-footer responsivo">
							<?php
								wp_nav_menu(
									array(
										'menu'           => 'footer-responsivo',
										'theme_location' => 'footer-responsivo',
										'depth'          => 2,
										'container'      => '',
										'menu_id'        => 'one-header',
										'fallback_cb'    => 'wp_bootstrap_navwalker::fallback',
										'walker'         => new wp_bootstrap_navwalker()
									)
								);
							?>
						</nav>
					</div>
					<div class="col-xs-12 col-md-5 col-lg-6">
						<div class="logo-footer">
							<a href="<?php echo SITEURL?>"><img src="<?php echo THEMEURL ?>/assets/img/logo-branca.svg" alt=""></a>
						</div>
						<div class="redes-sociais">
							<a href="https://www.facebook.com/Maxx-Net-Telecom-322785008108999/?fref=ts" target="_blank"><i class="maxxnet-facebook"></i></a>
							<a href="https://www.instagram.com/maxxnettele/" target="_blank"><i class="maxxnet-instagram"></i></a>
						</div>
					</div>

					<div class="col-xs-12">
						<div class="copy">
							<span>Maxxnet <?php echo date('Y'); ?> &copy; Todos os direitos reservados
							</span>
						</div>
					</div>
				</div>
			</div>
		</footer><!-- FIM FOOTER -->

		<?php wp_footer();?>
	</body>
</html>