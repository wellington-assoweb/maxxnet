<?php get_header(); ?>
<?php include(TEMPLATEPATH . '/template-parts/titulo-blog.php'); ?>

<section class="blog-page">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-9">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<div class="col-sm-12 col-md-6">
								<article class="um-post menor">
									<a href="<?php the_permalink() ?>" class="img-link">
										<?php if (has_post_thumbnail($post->ID )){
											$back = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
											$img_destak = $back['0']; ?>
											<div class="img-destak" style="background-image:url('<?php echo $img_destak ?>') !important">
										<?php }else{ ?>
											<div class="img-destak" style="background-image:url('<?php echo get_template_directory_uri(); ?>/assets/img/img-destaque-padrao.jpg')">
										<?php } ?>
											<div class="overlayer"></div>
											<div class="categoria">
												<?php
													$categories = get_the_category();
													$output = '';
													$cont = 0;//contador
													if($categories){
														foreach($categories as $category) {
															if ($cont == 1) {//Limita categorias
																break;
															}
															$output .= '<a href="'.get_category_link( $category->term_id ).'" class="hvr-pulse-grow" >'.$category->cat_name.'</a>';
															$cont++;
														}
														echo trim($output);
													}
												?>
											</div><!-- /.categoria -->
										</div>
									</a>
									<div class="info-post">
										<h2 class="headline"><a href="<?php the_permalink() ?>"><?php the_title()?></a></h2>
										<?php the_content_limit(110);?>
									</div>
								</article>
							</div>
		 		<?php endwhile?>
			</div>
			<div class="col-xs-12 col-sm-offset-3 col-sm-6 col-md-offset-0 col-md-3">
				<?php include(TEMPLATEPATH . '/template-parts/sidebar.php'); ?>
			</div>
			<?php else: ?>
				<div class="col-xs-12 col-sm-12 col-md-9">
					<div class="searchResult">
						<h3>Nenhum resultado para <i>"<?php echo $_GET['s'];?></i>"</h3>
						<p>Lamentamos mas não foram encontradas publicações para esse termo.</p>
						<p>Tente outra busca ou utilize o menu no topo do site</p>
					</div>
				</div>
				<div class="col-xs-12 col-sm-offset-3 col-sm-6 col-md-offset-0 col-md-3">
					<?php include(TEMPLATEPATH . '/template-parts/sidebar.php'); ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</section>
<?php get_footer(); ?>