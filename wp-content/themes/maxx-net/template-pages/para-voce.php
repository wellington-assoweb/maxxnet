<?php /* Template name: Para você */ get_header(); ?>
<?php include(TEMPLATEPATH . '/template-parts/titulo-comum.php'); ?>
<section class="primeira-section">
	<img class="img-esquerda" src="<?php echo THEMEURL; ?>/assets/img/bg-video-para-voce-esquerda.jpg" alt="">
	<img class="img-direita" src="<?php echo THEMEURL; ?>/assets/img/bg-video-para-voce-direita.jpg" alt="">
	<div class="my-container" style="position:relative; z-index:1">
		<div class="row">
			<div class="col-xs-12 col-md-offset-8 col-md-4">
				<h2><?php echo get_field('titulo_primeira_section'); ?><br/><span class="font-weight-bold">maxx <span class="color-verde">fibra</span></span></h2>
			</div>
			<div class="col-xs-12 col-md-offset-8 col-md-4 col-lg-offset-9 col-lg-3">
				<?php echo get_field('conteudo_primeira_section'); ?>
			</div>
		</div>
	</div>

	<video class="video" autoplay loop poster="<?php echo THEMEURL; ?>/assets/img/screenshot-video-para-voce.png">
		<source src="<?php echo THEMEURL; ?>/assets/video/para_voce.webm">
	</video>
</section>
<section class="planos planos-residenciais" id="planos-fibra">
	<div class="my-container" style="position:relative;">
		<div class="row">
			<div class="col-xs-12">
				<h2><?php echo get_field('titulo_dos_planos'); ?></h2>
			</div>
			<?php
				$cont = 1;
				$delay = .2;
				while (have_rows('planos_residenciais')): the_row();
					$quantidade_de_megas = get_sub_field('quantidade_de_megas');
					$reais = get_sub_field('reais');
					$centavos = get_sub_field('centavos');
					$preço_promocional = get_sub_field('preço_promocional');

					$download = get_sub_field('download');
					$upload = get_sub_field('upload');

					$beneficio_1 = get_sub_field('beneficio_1');
					$beneficio_2 = get_sub_field('beneficio_2');

					$id_plano = get_sub_field('id_plano');
					$detalhes = get_sub_field('detalhes');
			?>
					<div class="col-xs-12 col-sm-6 col-md-4 wow fadeInUp" data-wow-delay="<?php echo $delay; ?>s">
						<div class="plano plano-residencial ajus">
							<div class="qtd-uni">
								<span class="qtd"><?php echo $quantidade_de_megas ?></span>	<br><span class="uni">MEGA</span>
							</div>
							<div class="box">
								<div class="valor">
									<?php if($reais == ''){ ?>
										<span class="cifrao">Consulte</span>
									<?php }else{ ?>
										<span class="cifrao">R$</span>
										<span class="reais"><?php echo $reais ?></span>
										<span class="centavos">,<?php echo $centavos ?><?php if($preço_promocional){ ?>*<?php } ?></span>
										<span class="mes">/mês</span>
									<?php } ?>
								</div>
								<div class="mega-up">

									<div><b><?php if($download){ echo $download ?></b> mega de download<?php }else { ?>-</b><?php } ?></div>
									<div><b><?php if($download){ echo $upload ?></b> mega de upload<?php }else { ?>-</b><?php } ?></div>
								</div>
								<div class="wifi-sup">
									<?php  if ($beneficio_1) { ?>
										<div><?php echo $beneficio_1 ?></div>
									<?php  } ?>
									<?php  if ($beneficio_2) { ?>
										<div><?php echo $beneficio_2 ?></div>
									<?php  } ?>
								</div>
								<div class="botao">
									<a href="<?php echo SITEURL ?>/contato?id=<?php echo $id_plano; ?>&tipo=residencial">
										<span>CONTRATE</span>
									</a>
								</div>
								<div class="detalhes"><a href="#" class="detail" id="res<?php echo $cont; ?>" data-detalhes="<?php echo $detalhes ?>" data-mega="<?php echo $quantidade_de_megas ?>">DETALHES</a></div>
							</div>
						</div>
					</div>
			<?php
					$delay = $delay + .2;
					$cont++;
					if($delay == .10){
						$delay = .2;
					}
				endwhile;
			?>
		</div>
	</div>
</section>

<section class="vantagem">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12"><h2><?php echo get_field('titulo_vantagens') ?></h2></div>
			<?php
				$count = .3;
				while(have_rows('vantagens')): the_row();
					$imagem = get_sub_field('imagem');
					$titulo = get_sub_field('titulo');
					$conteudo = get_sub_field('conteudo');
			?>

				<div class="col-xs-12 col-md-4 wow fadeInLeft" data-wow-delay="<?php echo $count; ?>s">
					<div class="box">
						<img src="<?php echo $imagem['url']; ?>" alt="<?php echo $imagem['alt']; ?>" title="<?php echo $imagem['title']; ?>">
						<h3><?php echo $titulo; ?></h3>
						<?php echo $conteudo; ?>
					</div>
				</div>
			<?php $count= $count + .3; endwhile; ?>
		</div>
	</div>
</section>
<section class="chamada-outro-plano">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-lg-4">
				<h3><?php echo get_field('chamada_planos_empresariais') ?></h3>
				<div class="botao">
					<a href="<?php echo SITEURL ?>/para-sua-empresa">
						<span>PLANOS EMPRESARIAIS</span>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- MODAL -->
<div class="mask" role="dialog"></div>
<div class="modal2" role="alert">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12 col-md-6 grid-modal">
			    <div class="head-modal">
			        <button class="close" role="button"><i>X</i></button>
			    </div>
			    <div class="body-modal">
				    <hr>
			        <h3>Detalhes do plano</h3>
			        <p></p>
			        <div></div>
			    </div>
			    <div class="converse-conosco">
			        <div class="location">
			            <span><a href="<?php echo SITEURL;?>/contato"><i class="fiber-mail"></i> Converse conosco</a></span>
			        </div>
				    <hr>
			    </div>
			    <div class="foot-modal">
			    </div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>
<script>
	// to top right away
	if ( window.location.hash ) scroll(0,0);
	// void some browsers issue
	setTimeout( function() { scroll(0,0); }, 1);

	(function($) {
	    if(window.location.hash) {
	        $('html,body').animate({scrollTop:$(window.location.hash).offset().top-80}, 900);
	    }
	})(jQuery);
</script>