<?php /* Template name: Indicou Ganhou */ get_header(); ?>
<?php include(TEMPLATEPATH . '/template-parts/titulo-comum.php'); ?>
<section class="indicou">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12 col-md-8 centering">
				<h2><?php echo get_field('titulo') ?></h2>
			</div>
			<div class="col-xs-12 col-md-6 centering">
				<?php echo get_field('conteudo') ?>
			</div>
			<div class="clearfix"></div>

			<div class="col-xs-12 col-md-6 centering">
				<?php  echo do_shortcode('[contact-form-7 id="355" title="Programa indicou ganhou"]'); ?>
		</div>
	</div>
</section>
<?php get_footer(); ?>
<script>
	(function($) {
      var SPMaskBehavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
      },
      spOptions = {
        onKeyPress: function(val, e, field, options) {
            field.mask(SPMaskBehavior.apply({}, arguments), options);
          }
      };

      $('input[type="tel"]').mask(SPMaskBehavior, spOptions);
      $('.cpf').mask('000.000.000-00');
	})(jQuery);
</script>