<?php /* Template name: Central do Assinante */ get_header(); ?>
<?php include(TEMPLATEPATH . '/template-parts/titulo-comum.php'); ?>
<section class="central-do-assinante">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12 col-md-6">

				<h2>Perguntas Frequentes</h2>

				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					<?php
						$count= 1;
						while (have_rows('perguntas_frequentes')): the_row();
							$pergunta = get_sub_field('pergunta');
							$resposta = get_sub_field('resposta');
					?>
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="heading<?php echo $count ?>">
									<h4 class="panel-title">
										<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $count ?>" aria-expanded="true" aria-controls="collapse<?php echo $count ?>">
											<?php echo $pergunta ?>
										</a>
									</h4>
								</div>
								<div id="collapse<?php echo $count ?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading<?php echo $count ?>">
									<div class="panel-body">
										<?php echo $resposta ?>
									</div>
								</div>
							</div>
					<?php $count++; endwhile; ?>
				</div>
			</div>
			<div class="col-xs-12 col-md-6 col-box-contato">
					<h2><?php echo get_field('titulo') ?></h2>
				<div class="login">
					<form class="form-central" id="frmLogin" name="frmLogin" action="http://177.39.121.247:8080/sac/open.do?action=open&sys=SAC" method="get">
					<input type="hidden" name="sys" value="SAC"/>
					<input type="hidden" name="action" value="openform"/>
					<input type="hidden" name="formID" value="73"/>
					<input type="hidden" name="align" value="0"/>
					<input type="hidden" name="mode" value="-1"/>
					<input type="hidden" name="goto" value="-1"/>
					<input type="hidden" name="filter" value=""/>
					<input type="hidden" name="scrolling" value="no"/>
					<!--Campos de entrada de usuário e senha-->
						<div class="campo">
							<input type="text"  placeholder="Usuário" name="usuario" autocomplete="off" value=""/>
						</div>
						<div class="campo">
							<input type="text" name="senha" placeholder="Senha"  autocomplete="off" value=""/>
						</div>
						<div class="botao">
							<input value="Acessar" type="submit">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>



<?php get_footer(); ?>