<?php /* Template name: Sobre */ get_header(); ?>
<?php include(TEMPLATEPATH . '/template-parts/titulo-comum.php'); ?>
<section class="sobre">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12 col-md-6 float-right">
				<img class="logo-sobre" src="<?php echo THEMEURL ?>/assets/img/logo.svg" alt="">
			</div>
			<div class="col-xs-12 col-md-6 float-left">
				<h2><?php echo get_field('titulo'); ?></h2>
				<?php echo get_field('conteudo'); ?>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>