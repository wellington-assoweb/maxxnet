<?php /* Template name: Contato */ get_header(); ?>
<?php include(TEMPLATEPATH . '/template-parts/titulo-comum.php'); ?>
<section class="contato">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12 col-md-6 col-box-contato">
				<div class="box-contato">
					<svg version="1.1" id="envelope" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 73.4 75.6" style="enable-background:new 0 0 73.4 75.6;" xml:space="preserve">
						<path class="st1" d="M71.1,74.6H2.3c-0.7,0-1.3-0.5-1.3-1.1V31.7c0-0.4,0.3-0.8,0.7-1c0.4-0.2,0.9-0.1,1.3,0.1l33.7,22.4
										l33.7-22.4c0.4-0.3,0.9-0.3,1.3-0.1c0.4,0.2,0.7,0.6,0.7,1v41.7C72.4,74.1,71.8,74.6,71.1,74.6z M3.5,72.3h66.3V34L37.4,55.5
										c-0.4,0.3-1.1,0.3-1.5,0L3.5,34V72.3z"/>
						<g class="folha">
							<path class="st1" d="M65,31.7c-0.7,0-1.3-0.5-1.3-1.1v-13l-16-14.3H11.4v24.6c0,0.6-0.6,1.1-1.3,1.1c-0.7,0-1.3-0.5-1.3-1.1
								V2.1C8.9,1.5,9.5,1,10.2,1h38c0.3,0,0.7,0.1,0.9,0.3l16.8,14.9c0.2,0.2,0.4,0.5,0.4,0.8v13.5C66.2,31.2,65.7,31.7,65,31.7z"/>
							<path class="st1" d="M65,18.2H48.2c-0.7,0-1.3-0.5-1.3-1.1V2.1c0-0.5,0.3-0.9,0.8-1c0.5-0.2,1-0.1,1.4,0.2l16.8,14.9
								c0.4,0.3,0.5,0.8,0.3,1.2C65.9,17.9,65.5,18.2,65,18.2z M49.4,15.9h12.5L49.4,4.8V15.9z"/>
							<path class="st1" d="M10.2,38.1c-0.7,0-1.3-0.5-1.3-1.1v-9.2c0-0.6,0.6-1.1,1.3-1.1c0.7,0,1.3,0.5,1.3,1.1V37
										C11.4,37.6,10.9,38.1,10.2,38.1z"/>

							<path class="st1" d="M56.6,22.8h-39c-0.7,0-1.3-0.5-1.3-1.1c0-0.6,0.6-1.1,1.3-1.1h39c0.7,0,1.3,0.5,1.3,1.1
									C57.8,22.3,57.3,22.8,56.6,22.8z"/>
							<path class="st1" d="M32.7,16.2H17.6c-0.7,0-1.3-0.5-1.3-1.1c0-0.6,0.6-1.1,1.3-1.1h15.1c0.7,0,1.3,0.5,1.3,1.1
									C34,15.7,33.4,16.2,32.7,16.2z"/>
							<path class="st1" d="M56.6,30h-39c-0.7,0-1.3-0.5-1.3-1.1c0-0.6,0.6-1.1,1.3-1.1h39c0.7,0,1.3,0.5,1.3,1.1
									C57.8,29.5,57.3,30,56.6,30z"/>
							<path class="st1" d="M56.6,37.1h-39c-0.7,0-1.3-0.5-1.3-1.1c0-0.6,0.6-1.1,1.3-1.1h39c0.7,0,1.3,0.5,1.3,1.1
									C57.8,36.6,57.3,37.1,56.6,37.1z"/>
							<path class="st1" d="M71.1,32.8c-0.4,0-0.7-0.1-1-0.4L64,25.9c-0.4-0.5-0.4-1.2,0.2-1.6c0.5-0.4,1.3-0.3,1.8,0.1l6.1,6.5
								c0.4,0.5,0.4,1.2-0.2,1.6C71.7,32.8,71.4,32.8,71.1,32.8z"/>
							<path class="st1" d="M2.3,32.8c-0.3,0-0.7-0.1-0.9-0.4c-0.5-0.5-0.4-1.2,0.1-1.6l7.9-6.5c0.5-0.4,1.3-0.4,1.8,0.1
								c0.5,0.5,0.4,1.2-0.1,1.6l-7.9,6.5C2.9,32.7,2.6,32.8,2.3,32.8z"/>
						</g>
						<g  class="borda">
							<path class="st2" d="M37.8-7"/>
							<path class="st2" d="M-29.3,31.4"/>
							<path class="st2" d="M-5.7,34.1"/>
						</g>
						<g  class="borda">
							<path class="st1" d="M1,73.5L1,31.8c0-0.6,0.6-1.1,1.3-1.1l68.9-0.2c0.7,0,1.3,0.5,1.3,1.1l0.1,41.7c0,0.4-0.3,0.8-0.7,1
								c-0.4,0.2-0.9,0.2-1.3-0.1 M36.7,52 M69.9,71.2l-0.1-38.4L3.5,32.9l0.1,38.4"/>
						</g>
						<path  id="one" class="st0" d="M3.5,72.3h66.3V34L37.4,55.5c-0.4,0.3-1.1,0.3-1.5,0L3.5,34V72.3z"/>
					</svg>
					<?php  echo do_shortcode('[contact-form-7 id="122" title="contato Dinamico"]'); ?>
				</div>
			</div>
			<div class="col-xs-12 col-md-6">
				<h2><?php echo get_field('titulo') ?></h2>
				<div class="tel-end">
					<div class="telefone">
						<div><i class="maxxnet-icon-tel24hrs"></i></div>
						<div>0800 940 5770 <br>(31) 3336 9070</div>
					</div>
					<div class="endereco">
						<div><img src="<?php echo THEMEURL ?>/assets/img/icon-endereco.svg" alt=""></div>
						<div>Av. Senador Levindo Coelho, 3320<br>Vale do Jatoba, Belo Horizonte - MG</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>

<?php
	$args = array(
		'page_id' => 42,
		'order' => 'ASC',
		'posts_per_page' => -1
	);
?>
<script>
	(function($) {
	var planosResidenciais = [];
	var planosEmpresariais = [];
	var planosRadio = [];
	<?
		$idGet = $_GET['id'];
		$categoriaGet = $_GET['tipo'];
		if($idGet == ''){
			$idGet = 0;
		}
		if($categoriaGet == ''){
			$categoriaGet = 'residencial';
		}

	    $posts = new WP_Query( $args );

		//Planos Residenciais
		while ( $posts->have_posts() ) : $posts->the_post();
			while ( have_rows('planos_residenciais') ) : the_row();
				$id = get_sub_field('id_plano');
				$nomePlano = get_sub_field('nome_do_plano');
				$download = 'Download: '.$velocidadeDow.$unidadeDown;

				$velocidadeUp = get_sub_field('velocidade_upload');
				$unidadeUp = get_sub_field('unidade_upload');
				$upload = 'Upload: '.$velocidadeUp.$unidadeUp;
			?>
				construtorPlanosRes(<?php echo $id ?>, '<?php echo $nomePlano ?>');
			<?php
			endwhile;
		endwhile;
		wp_reset_query();


		//Planos Empresarias
		$args = array(
			'page_id' => 65,
			'order' => 'ASC',
			'posts_per_page' => -1
		);
		$posts = new WP_Query( $args );
		while ( $posts->have_posts() ) : $posts->the_post();
			while ( have_rows('planos_empresariais') ) : the_row();
				$id = get_sub_field('id_plano');
				$nomePlano = get_sub_field('nome_do_plano');
			?>
				construtorPlanosEmp(<?php echo $id ?>, '<?php echo $nomePlano ?>');
			<?php

			endwhile;
		endwhile;
		wp_reset_query();


		//Planos Residenciais Radio
		$args = array(
			'page_id' => 173,
			'order' => 'ASC',
			'posts_per_page' => -1
		);
		$posts = new WP_Query( $args );
		while ( $posts->have_posts() ) : $posts->the_post();
			while ( have_rows('planos_residenciais') ) : the_row();
				$id = get_sub_field('id_plano');
				$nomePlano = get_sub_field('nome_do_plano');
			?>
				construtorPlanosRadio(<?php echo $id ?>, '<?php echo $nomePlano ?>');
			<?php

			endwhile;
		endwhile;
		wp_reset_query();
	?>

		if('<?php echo $categoriaGet ?>' == 'residencial'){
			montaSelectLoad(planosResidenciais);
			var opcaoTipo1 = $("#selectTipo option[value=Residencial]");
			opcaoTipo1.attr('selected','selected');
		}else if('<?php echo $categoriaGet ?>' == 'empresarial'){
			montaSelectLoad(planosEmpresariais);
			var opcaoTipo2 = $("#selectTipo option[value=Empresarial]");
			opcaoTipo2.attr('selected','selected');
		}else{
			montaSelectLoad(planosRadio);
			var opcaoTipo2 = $("#selectTipo option[value=Radio]");
			opcaoTipo2.attr('selected','selected');
		}
		function construtorPlanosRes(id, nomePlano){
			plano = {
				'id': id,
				'nomePlano': nomePlano
			}
			planosResidenciais.push(plano);
		}

		function construtorPlanosEmp(id, nomePlano){
			plano = {
				'id': id,
				'nomePlano': nomePlano
			}
			planosEmpresariais.push(plano);
		}
		function construtorPlanosRadio(id, nomePlano){
			plano = {
				'id': id,
				'nomePlano': nomePlano
			}
			planosRadio.push(plano);
		}

		function montaSelectLoad(planos){

			for (var i = 0; i < planos.length; i++) {
				var valuePlano = planos[i].nomePlano;

				$('#selectPlano').append($('<option>', {
			        value: valuePlano,
			        text : valuePlano,
			        id: planos[i].id
			    }));

				if(planos[i].id == <?php echo $idGet ?>){

					var opcaoPlano = $("#"+planos[i].id);
					opcaoPlano.attr('selected','selected');
				}
			}
		}


		$("select[name=menu-17]").change(
	        function() {
	        	var tipoPlano = $('select[name=menu-387] option:selected').text();
	        	if(tipoPlano == 'Empresarial'){
	            	mudancaOpcaoPlano(planosEmpresariais);
	        	}else if(tipoPlano == 'Residencial'){
	            	mudancaOpcaoPlano(planosResidenciais);
	        	}else{
	            	mudancaOpcaoPlano(planosRadio);
	        	}
	        }
	    );

	    $("select[name=menu-387]").change(
	        function() {
	        	var tipoPlano = $('select[name=menu-387] option:selected').text();
	        	if(tipoPlano == 'Empresarial'){
	            	refreshPlanos(planosEmpresariais);
	        	}else if(tipoPlano == 'Residencial'){
	            	refreshPlanos(planosResidenciais);
	        	}else{
	        		refreshPlanos(planosRadio);
	        	}
	        }
	    );

	    function refreshPlanos(planos){
				$('#selectPlano option').remove();
				$('#selectPlano').append($('<option value="">Selecione o plano...</option>'));
	    	for (var i = 0; i < planos.length; i++) {
				var valuePlano = planos[i].nomePlano;
				$('#selectPlano').append($('<option>', {
			        value: valuePlano,
			        text : valuePlano,
			        id: planos[i].id
			    }));
			}
	    }

		function mudancaOpcaoPlano(plano){
			for (var i = 0; i < plano.length; i++) {
				var opcaoSelecionada = $('select[name=menu-17] option:selected').text();
				if(opcaoSelecionada == plano[i].nomePlano){
					var opcao = $("#"+plano[i].id);
					opcao.attr('selected','selected');
				}
			}
		}
	})(jQuery);
</script>

<script>
	(function($) {
      var SPMaskBehavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
      },
      spOptions = {
        onKeyPress: function(val, e, field, options) {
            field.mask(SPMaskBehavior.apply({}, arguments), options);
          }
      };

      $('input[type="tel"]').mask(SPMaskBehavior, spOptions);
      $('.cpf').mask('000.000.000-00');
	})(jQuery);
</script>