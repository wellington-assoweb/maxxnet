<?php get_header(); /* Template name: Teste sua Velocidade */ ?>
<?php include(TEMPLATEPATH . '/template-parts/titulo-comum.php'); ?>
<section class="velocimetro">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-6">
				<div class="box">
					<h2><?php the_field('subtitulo_ao_lado_do_velocimetro'); ?></h2>
					<?php the_field('breve_desc'); ?>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6">
				<div class="velocity">
					<iframe src="http://speedtest.copel.net/speedtest_geral_new.html" frameborder="0" height="100%" width="100%" wmode="Opaque"></iframe>
				</div>
			</div>
		</div>
	</div>
</section>
<div id="modal" class="modal fade in" tabindex="-1" role="dialog" style="display: block;" aria-hidden="false">
	<div class="overlay"></div>
	<div class="modal-dialog modal-dialog-g">
		<div class="modal-content">
			<div class="modal-body">
				<h6>Dicas importantes para realizar o teste</h6>
				<div class="little">
					<p>
						<h3>Antes de medir a velocidade da sua banda, siga estas dicas para que o resultado seja o mais preciso possível:</h3>
					</p>
				</div>
				<ul class="list-dicas">
					<li>
						<img src="<?php echo THEMEURL ?>/assets/img/icone-evite-redes-sem-fio.png" alt=""><br>
						Evite redes sem fio. Faça o teste em um computador conectado a internet por meio de um cabo de rede;
					</li>
					<li>
						<img src="<?php echo THEMEURL ?>/assets/img/icone-feche-os-programas.png" alt=""><br>
						Feche todos os programas;
					</li>
					<li>
						<img src="<?php echo THEMEURL ?>/assets/img/icone-cancelar-atualizacoes.png" alt=""><br>
						Interrompa as atualizações;
					</li>
					<li>
						<img src="<?php echo THEMEURL ?>/assets/img/icone-cabo-de-rede.png" alt=""><br>
						Mantenha somente o computador conectado na rede. Desconecte todos os demais dispositivos conectados em sua rede, seja via cabo ou Wi-Fi;
					</li>
					<li>
						<img src="<?php echo THEMEURL ?>/assets/img/icone-repita-o-teste.png" alt=""><br>
						Repita o teste.
					</li>
				</ul>
				<div class="text-center row modal-bts">
					<div class="col-sm-10 col-sm-offset-1 col-xs-12">
						<div class="col-xs-12">
							<div class="botao">
								<button id="fechar-modal"><span>Obrigado. Quero fazer o teste.</span></button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>
<script>
	(function($) {
		$('#fechar-modal').click(function(){
			$('#modal').css('display', 'none');
			$('.velocimetro .velocity').css('display', 'block');
		});
	})(jQuery);
</script>