<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="theme-color" content="#73C5D0">
		<title><?php the_title();?></title>

		<!--Favicon-->
		<link rel="shortcut icon" href="<?php echo THEMEURL; ?>/assets/img/favicon.png" >
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-152x152.png">
		<?php include(TEMPLATEPATH . '/template-parts/loop-style.php'); ?>
		<?php wp_head();?>

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
			<!--[if IE ]>
				<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
				<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
				<link rel="stylesheet" type="text/css" href="ie.css" />
			<![endif]-->


			<!-- Facebook Pixel Code -->
			<script>
				!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
				n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
				n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
				t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
				document,'script','https://connect.facebook.net/en_US/fbevents.js');

				fbq('init', '1773413886232016');
				fbq('track', "PageView");

			</script>
			<noscript><img height="1" width="1" style="display:none"
			src="https://www.facebook.com/tr?id=1773413886232016&ev=PageView&noscript=1"
			/></noscript>
	</head>
	<body <?php body_class();?>>
		<div id="wrap">
			<header class="header">
				<div id="header-sroll">
					<div class="my-container">
						<div class="row">
							<div class="desk-menu">
								<div class="box-menu">
									<div class="col-xs-3">
										<div class="logo">
											<?php
												if(is_front_page()){?>
													<h1 class="logo-adn">
														<a class="siteLogo" data-g-label="Maxx Net Telecom " title="Maxx Net Telecom" href="<?php echo get_bloginfo('url');?>">Maxx Net Telecom</a>
													</h1>
												<?php }else{ ?>
													<span class="logo-adn">
														<a class="siteLogo" data-g-label="Maxx Net Telecom " title="Maxx Net Telecom" href="<?php echo get_bloginfo('url');?>">Maxx Net Telecom</a>
													</span>
												<?php }
											?>
										</div>
									</div>
									<div class="col-xs-6">
									</div>
										<nav class="menu-one">
											<?php
												wp_nav_menu(
													array(
														'menu'           => 'Header',
														'theme_location' => 'header',
														'depth'          => 2
													)
												);
											?>
										</nav>
									<div class="col-xs-3">
										<div class="telefones tel-norm">
											<div class="telefone">0800 940 5770</div>
											<div class="telefone-fix"><i class="maxxnet-icon-tel24hrs"></i>(31) 3336 9070</div>
										</div>
										<div class="redes-sociais">
											<a href="https://www.facebook.com/Maxx-Net-Telecom-322785008108999/?fref=ts" target="_blank"><i class="maxxnet-facebook"></i></a>
										</div>
										<div class="hamburger-menu" id="open-button">
			  								<div class="bar"></div>
			  							</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="responsive-menu">
						<nav class="bx-menu">
							<?php
								wp_nav_menu(
									array(
										'menu'           => 'header-responsivo',
										'theme_location' => 'header-responsivo',
										'depth'          => 2
									)
								);
							?>
							<div class="telefones tel-norm">
								<div class="telefone"><i class="maxxnet-icon-tel24hrs"></i>0800 940 5770</div>
								<div class="telefone-fix">(31) 3336 9070</div>
							</div>
						</nav>
					</div>
				</div>
			</header><!-- /.header -->