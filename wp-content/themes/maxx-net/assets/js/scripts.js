(function($) {

    //MODAL
    $(".detail").on("click", function(e){
        e.preventDefault();
        $(".mask").addClass("active");

        var plano = $(this).attr('id');
        var mega = $(this).attr('data-mega');
        var detalhes = $(this).attr('data-detalhes');
        $('.body-modal p').text( 'Plano de  '+mega+' Mega' );
        $('.body-modal div').html( detalhes );
    });




    function closeModal(){
        $(".mask").removeClass("active");
    }
    $(".close, .mask").on("click", function(){
        closeModal();
    });


    //Hamburger menu addClass for animate
    $('.hamburger-menu').on('click', function() {
        $('.bar').toggleClass('animate');
    })

     //Smaller header when scroll
    $(window).scroll(function () {
         var sc = $(window).scrollTop()
        if (sc > 40) {
            $("#header-sroll").addClass("small")
        }else {
            $("#header-sroll").removeClass("small")
        }
    });


    var tl = 0;
    var magnitude = 0.3;

    addEventListener('scroll', function(){
        var top = $(document).scrollTop();
        interp(top);
    });

    function interp(t){
        var x = t-(tl-t);
        $('.slide').css({top: x*magnitude});
    }


    /* MENU SCRIPT */
    var openbtn = document.getElementById( 'open-button' ),
        headscroll = document.getElementById( 'header-sroll' ),
        isOpen = false;

    function init() {
        initEvents();
    }

    function initEvents() {
        openbtn.addEventListener( 'click', toggleMenu );
    }
    function toggleMenu() {
        if( isOpen ) {
            classie.remove( headscroll, 'show-menu' );
        }
        else {
            classie.add( headscroll, 'show-menu' );
        }
        isOpen = !isOpen;
    }
    init();

    function rodape(){
        var footerHeight = $('.footer').height();
        jQuery('.footer').css('margin-top', -(footerHeight)+"px");
        jQuery('.conteudo').css('padding-bottom', (footerHeight + 80)+"px");
    };

    jQuery(document).ready(function(){
         var current = '.mtlsr-images-for-lightbox ul li.current';

        //Função para verificar a posição atual da imagem e remover as setas(prev e next) conforme necessário
        function check_image_position(){
            if ($(current).is(':last-child')) {
                $('.next').hide();
                $('.prev').show();
            }else if ($(current).is(':first-child')) {
                $('.next').show();
                $('.prev').hide();
            }else{
                $('.next, .prev').show();
            }
        }

        $('.mtlsr-images-for-lightbox ul li').on('click', 'a', function(event) {
            event.preventDefault();
            var big_image_href = $(this).attr('href');

            $(this).parent().addClass('current');
            $('.mtlsr-lightbox').fadeIn();
            $('.mtlsr-lightbox').append('<img class="image-in-lightbox" src="'+big_image_href+'" alt=""></div>');

            check_image_position();
        });
        //Fechar
        $('.mtlsr-lightbox').on('click', '.close', function(event) {
            $('.mtlsr-lightbox').fadeOut();
            $('.mtlsr-lightbox .image-in-lightbox').remove();
            $(current).removeClass('current');
        });
        //Função Next e Prev
        $('.mtlsr-lightbox a').on('click', function(e){
            if($(this).attr('class')=='next'){
                var big_image_href = $(current).next().find('a').attr('href');

                $(current).next().addClass('current');
                $(current).prev().removeClass('current');

            }else if($(this).attr('class')=='prev'){
                var big_image_href = $(current).prev().find('a').attr('href');

                $(current).prev().addClass('current');
                $(current).next().removeClass('current');
            }
            check_image_position();

            $('.mtlsr-lightbox .image-in-lightbox').remove();
            $('.mtlsr-lightbox').append('<img class="image-in-lightbox" src="'+big_image_href+'" alt=""></div>');
        });
    });

})(jQuery);