(function($) {
 	jQuery(window).load(function () {
	    var videoParaVoce = document.getElementById("video-para-voce");
		videoParaVoce.muted = true;
	 });


	var loadAgain = true;
	var firsLoad = true;

    var scrollVideo = $(".comparador").offset().top;

	$(window).scroll(function() {
        if (loadAgain && $(window).scrollTop() >= scrollVideo-200) {
        	if(firsLoad){
	            document.getElementById('video-comparador').play();
	            firsLoad = false;
	        }
        }
    });

})(jQuery);