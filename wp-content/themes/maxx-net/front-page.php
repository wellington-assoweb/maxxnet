<?php /* Template name: Página Inicial */ get_header('home'); ?>
<section class="banner">
	<div class="my-container">
		<div class="row">
			<div class="group-banner group-banner-normal">
				<div class="col-xs-12 col-sm-10 col-md-offset-2 col-md-7 col-lg-6">
					<h2 class="title-banner"><span class="color-cinza">MAXX</span> <span class="color-verde">FIBRA</span></h2>
					<div class="mega-e-preco">
						<?php
							while(have_rows('banner')): the_row();
								$quantidade_de_mega = get_sub_field('quantidade_de_mega');
								$reais = get_sub_field('reais');
								$centavos = get_sub_field('centavos');
							endwhile;
						?>
							<span class="qtd-mega wow fadeInLeft"><?php echo $quantidade_de_mega ?></span>
							<span class="mega color-cinza wow fadeInLeft" data-wow-delay=".3s">mega</span>
							<span class="por color-cinza wow fadeIn" data-wow-delay=".6s">por</span>
							<span class="rs wow fadeIn" data-wow-delay="1s">R$</span>
							<span class="reais wow fadeIn" data-wow-delay="1s"><?php echo $reais ?></span>
							<span class="centavos wow fadeIn" data-wow-delay="1s">,<?php echo $centavos ?><span class="asterisco">*</span></span>
							<a href="<?php echo SITEURL ?>/maxx-fibra#planos-fibra"><span class="consulte">Consulte condições.</span></a>
					</div>
					<div class="suporte-e-wifi">
						<div class="vinte-e-quatro-suporte">
							<span class="numero">24</span>
							<span class="letra">H</span><br>
							<span class="de-suporte color-cinza">DE SUPORTE</span>
						</div>
						<div class="wifi-gratis">
							<span class="wifi">WI-FI</span><br>
							<span class="gratis color-cinza">GRÁTIS</span>
						</div>
					</div>
				</div>
			</div>
			<div class="group-banner group-banner-respon">
				<div class="col-xs-12">
					<h2 class="title-banner"><span class="color-cinza">MAXX</span> <span class="color-verde">FIBRA</span></h2>
					<div class="mega-e-preco">
						<span class="qtd-mega"><?php echo $quantidade_de_mega ?></span>
						<span class="mega color-cinza">mega</span>
						<span class="por color-cinza">por</span>
						<span class="rs">R$</span>
						<span class="reais"><?php echo $reais ?></span>
						<span class="centavos">,<?php echo $centavos ?><span class="asterisco">*</span></span>


					</div>
					<div class="suporte-e-wifi">
						<div class="vinte-e-quatro-suporte">
							<span class="numero">24</span>
							<span class="letra">H</span><br>
							<span class="de-suporte color-cinza">DE SUPORTE</span>
						</div>
						<div class="wifi-gratis">
							<span class="wifi">WI-FI</span><br>
							<span class="gratis color-cinza">GRÁTIS</span>
						</div>
					</div>
				</div>
				<a href="<?php echo SITEURL ?>/maxx-fibra#planos-fibra"><span class="consulte">Consulte condições.</span></a>
			</div>
			<div class="col-xs-12 col-sm-10 col-md-8">
				<div class="botao">
					<a href="<?php echo SITEURL ?>/contato">
						<span>Contratar</span>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="vantagem">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12"><h2>VANTAGENS</h2></div>
			<?php
				$count = .3;
				while(have_rows('vantagens')): the_row();
					$imagem = get_sub_field('imagem');
					$titulo = get_sub_field('titulo');
					$conteudo = get_sub_field('conteudo');
			?>

				<div class="col-xs-12 col-md-4 wow fadeInLeft" data-wow-delay="<?php echo $count; ?>s">
					<div class="box">
						<img src="<?php echo $imagem['url']; ?>" alt="<?php echo $imagem['alt']; ?>" title="<?php echo $imagem['title']; ?>">
						<h3><?php echo $titulo; ?></h3>
						<?php echo $conteudo; ?>
					</div>
				</div>

			<?php $count= $count + .3; endwhile; ?>
			<div class="col-xs-12">
				<div class="botao">
					<a href="<?php echo SITEURL ?>/contato"><span>contratar</span></a>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="para-voce">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12 col-md-2">
				<div class="box-esquerda">
					<h2 class="color-verde">JOGOS E<br> FILMES</h2>
					<h3>SEM INTERRUPÇÕES</h3>
				</div>
			</div>
			<div class="col-xs-12 col-md-6">
				<video  id="video-para-voce" autoplay muted loop poster="<?php echo THEMEURL; ?>/assets/img/TV-maxx-net-telecom.png">
					<source src="<?php echo THEMEURL ?>/assets/video/tv-na-maxx-net-telecom.webm" type='video/webm' />
					<source src="<?php echo THEMEURL ?>/assets/video/tv-na-maxx-net-telecom.mp4" type='video/mp4' />
				</video>
				<h4>MAXX <span class="color-verde">FIBRA</span></h4>
			</div>
			<div class="col-xs-12 col-md-4">
				<div class="box-direita">
					<h2 class="color-verde"><?php echo get_field('titulo_para_voce'); ?></h2>
					<?php echo get_field('conteudo_para_voce'); ?>
					<div class="box-plano-pra-vc">
						<?php
							while(have_rows('para_voce')): the_row();
								$quantidade_de_mega = get_sub_field('quantidade_de_mega');
								$reais = get_sub_field('reais');
								$centavos = get_sub_field('centavos');
							endwhile;
						?>
						<div class="qtd-e-unid">
							<span class="vc-qtd-mega"><?php echo $quantidade_de_mega; ?></span><br>
							<span class="vc-unidade">MEGA</span>
						</div>
						<div class="restante">
							<span class="vc-por">POR</span>
							<span class="vc-rs">R$</span>
							<span class="vc-reais"><?php echo $reais; ?></span>
							<span class="vc-centavos">,<?php echo $centavos; ?><span class="asterisco">*</span></span>

							<div class="box-wifi-gratis">
								<span class="vc-wifi">WI-FI</span><br>
								<span class="vc-gratis">GRÁTIS</span><br>
							</div>
						</div>
						<a href="<?php echo SITEURL ?>/maxx-fibra#planos-fibra"><span class="consulte-para-voce">Consulte condições</span></a>
					</div>
				</div>
				<div class="botao">
					<a href="<?php echo SITEURL ?>/maxx-fibra">
						<span>conheça outros planos</span>
					</a>
				</div>
			</div>
		</div>
		<div class="cachorros">
			<img src="<?php echo THEMEURL ?>/assets/img/assita-com-a-maxx-net.png" alt="">
		</div>
	</div>
</section>
<section class="para-sua-empresa">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-offset-1 col-md-5 col-lg-offset-2 col-lg-4 wow fadeIn" data-wow-duration="3s">
				<div class="box">
					<h3><?php echo get_field('titulo_para_sua_empresa'); ?></h3>
					<?php echo get_field('conteudo_para_sua_empresa'); ?>
					<div class="botao">
						<a href="<?php echo SITEURL ?>/para-sua-empresa">
							<span>Conhecer Planos</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="comparador">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12">
				<video id="video-comparador" poster="<?php echo THEMEURL; ?>/assets/img/TV-maxx-net-telecom.png">
					<source src="<?php echo THEMEURL ?>/assets/video/teste.webm" type="video/mp4">
				</video>
			</div>
			<div class="col-xs-12 botao">
				<button class="link" id="testar" onclick="document.getElementById('video-comparador').play()"><span>Teste novamente</span></button>
			</div>
		</div>
	</div>
</div>
<section class="indique">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12 col-md-offset-6 col-md-6">
				<div class="box">
					<h2><?php echo get_field('titulo_indique') ?></h2>
					<?php echo get_field('conteudo_indique') ?>
					<div class="botao">
						<a href="<?php echo SITEURL ?>/programa-indicou-gahou"><span>Saiba mais</span></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>