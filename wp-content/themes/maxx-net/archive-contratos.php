<?php get_header(); /* Template name: Termos promocionais */  ?>
<?php include(TEMPLATEPATH . '/template-parts/titulo-comum.php'); ?>
<section class="termos-promocionais">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12">
				<h2>Descubra como aproveitar ao máximo todos serviços Maxx Net.</h2>
			</div>
			<div class="box-aqurivos">
			<?php
				$args = array(
					'post_type' => 'contratos',
					'posts_per_page' => -1
				);
	            $loop = new WP_Query( $args );
				while ( $loop->have_posts() ) : $loop->the_post();

            ?>
				<div class="col-xs-12 col-sm-6 col-md-3">
					<a class="link-arquivo" href="<?php echo get_permalink(); ?>">
						<svg version="1.1" class="icon-arquivos" id="Layer_1" viewBox="0 0 60 60">
								<path d="M23.429,17H47c0.552,0,1-0.447,1-1s-0.448-1-1-1H23.429c-0.552,0-1,0.447-1,1S22.877,17,23.429,17z"/>
							<path d="M23.429,32H47c0.552,0,1-0.447,1-1s-0.448-1-1-1H23.429c-0.552,0-1,0.447-1,1S22.877,32,23.429,32z"/>
							<path d="M23.429,47H47c0.552,0,1-0.447,1-1s-0.448-1-1-1H23.429c-0.552,0-1,0.447-1,1S22.877,47,23.429,47z"/>
							<path d="M59,0H1C0.448,0,0,0.447,0,1v58c0,0.553,0.448,1,1,1h58c0.552,0,1-0.447,1-1V1C60,0.447,59.552,0,59,0z M58,58H2V2h56V58z"
								/>
							<polygon points="12.501,18.474 14.929,17.197 17.357,18.474 16.894,15.77 18.858,13.854 16.143,13.46 14.929,11 13.715,13.46
								11,13.854 12.965,15.77 	"/>
							<polygon points="12.501,33.557 14.929,32.28 17.357,33.557 16.894,30.853 18.858,28.938 16.143,28.543 14.929,26.083
								13.715,28.543 11,28.938 12.965,30.853 	"/>
							<polygon points="12.501,49 14.929,47.723 17.357,49 16.894,46.296 18.858,44.381 16.143,43.986 14.929,41.526 13.715,43.986
								11,44.381 12.965,46.296 	"/>
						</svg>
						<div class="nome-arquivo"><?php echo get_field('titulo_do_termo'); ?></div>
					</a>
				</div>
			<?php endwhile;  ?>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>