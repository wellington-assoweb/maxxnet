<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'db_maxxnet');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', '192.168.2.99');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ':!y19#fKv;(Otnic0Q2uk<MFu`S[nvxCtg>j?_)BnW(w$kkHntw>nPPS3#{xoh7<');
define('SECURE_AUTH_KEY',  'MGpUx]tx{ @JPrLMp.#rh4;RXAc8JPpg!~N?Q*6q;<A3C~$G`52&}48<u&U%l^b!');
define('LOGGED_IN_KEY',    '6.7XYRbHaStF8=jlQSNf>VJi:f`:WqLz-k%(t.m?j[BCq[p>rXlh]|Pj-x-d:hoQ');
define('NONCE_KEY',        't9KmV9;7&Fp)Pjy1Gc{9s4&h0qwQi#W;kG[%G{8KKT{&ga#G[}OV&7|4{.-CjP(,');
define('AUTH_SALT',        'kBD~vo[K/}Opj/ydK.p%1_#.RHE{V~@d_RQ(RuBR|g5c)TjI3`HPMzPnP]pK?&M8');
define('SECURE_AUTH_SALT', 'C{E_1wv?Q{OohAZ*+vYCP_#B=5T&`UR!35+e,p4xq&aNb=VubC}9,RE@!U>-6cY4');
define('LOGGED_IN_SALT',   ' qT)Ga79znrS4Y,)Ky6!wA@-zV{We^$g7kK.Im:s2Du!&`W&+FU0?A)!n4OFEH}$');
define('NONCE_SALT',       'anw.#!TgHTv{Bec tSBE%1P<NjRS{ly{4$9e#IS]@x*?qGk(3]*q~#x0)vz##+1I');

/**#@-*/
/* Constante */
define('THEMEURL',   'http://192.168.2.99/maxxnet/wp-content/themes/maxx-net');
define('SITEURL',       'http://192.168.2.99/maxxnet');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = '5cd13_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
